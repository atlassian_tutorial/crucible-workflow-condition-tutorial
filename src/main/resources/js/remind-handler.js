define('FECRU/workflow-condition-tutorial/remind-handler', ['require'], function (require) {
    'use strict';

    var $ = require('jquery');
    var fecru = require('global-ns/fecru');

    var onClickFunc = function (workflowConditionContext) {
        var url = fecru.pageContext + '/rest/workflowcondition-tutorial/latest/' + workflowConditionContext.reviewId;
        $.ajax({
            type: "get",
            url: url,
            dataType: "json",
            success: function () {
                workflowConditionContext.resolveMe();
            },
            error: function () {
                workflowConditionContext.showError();
            }
        });
    };
    return {
        onClick: onClickFunc
    };
});