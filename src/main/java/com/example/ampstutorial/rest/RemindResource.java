package com.example.ampstutorial.rest;

import com.atlassian.crucible.spi.PermId;
import com.atlassian.crucible.spi.data.DetailedReviewData;
import com.atlassian.crucible.spi.data.ReviewerData;
import com.atlassian.crucible.spi.services.ReviewService;
import com.atlassian.crucible.spi.services.ServerException;
import com.atlassian.crucible.spi.services.UserService;
import com.atlassian.fisheye.spi.data.MailMessageData;
import com.atlassian.fisheye.spi.services.MailService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.google.common.base.Throwables;
import com.sun.jersey.spi.resource.Singleton;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
@Singleton
@AnonymousAllowed
@Named
public class RemindResource {

    private final MailService mailService;
    private final ReviewService reviewService;
    private final UserService userService;

    @Inject
    public RemindResource(@ComponentImport MailService mailService,
                          @ComponentImport ReviewService reviewService,
                          @ComponentImport UserService userService) {
        this.mailService = mailService;
        this.reviewService = reviewService;
        this.userService = userService;
    }

    @GET
    @Path("{reviewId}")
    public Response sendReminder(@PathParam("reviewId") String reviewId) {
        if (!mailService.isConfigured()) {
            return Response.status(500).build();
        }

        final MailMessageData mailMessageData = new MailMessageData();
        mailMessageData.setSubject("Finish review " + reviewId + ",please");
        mailMessageData.setBodyText(MediaType.TEXT_PLAIN, "Could you please finish the review?");

        final DetailedReviewData reviewDetails = reviewService.getReviewDetails(new PermId<>(reviewId));
        reviewDetails.getReviewers()
                .reviewer
                .stream()
                .filter(reviewer -> !reviewer.isCompleted())
                .forEach(reviewer -> mailMessageData.addRecipient(getReviewerEmail(reviewer)));

        mailService.sendMessage(mailMessageData);

        return Response.ok().build();
    }

    private String getReviewerEmail(ReviewerData reviewer) {
        try {
            return userService.getUserProfile(reviewer.getUserName()).getEmail();
        } catch (ServerException e) {
            Throwables.propagate(e);
        }
        //not reachable
        return null;
    }

}
