package com.example.ampstutorial;

import com.atlassian.crucible.spi.PermId;
import com.atlassian.crucible.spi.data.ReviewData;
import com.atlassian.crucible.spi.services.ReviewService;
import com.atlassian.crucible.workflow.ResultAction;
import com.atlassian.crucible.workflow.ResultMessage;
import com.atlassian.crucible.workflow.ValidationResult;
import com.atlassian.crucible.workflow.WorkflowCondition;
import com.atlassian.crucible.workflow.WorkflowTransitionContext;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.ApplicationProperties;
import com.google.common.collect.Lists;

import javax.inject.Inject;
import javax.inject.Named;

@Named
public class CompletedReviewerWorkflowCondition implements WorkflowCondition {

    private static final String MODULE_KEY =
            "com.example.ampstutorial.workflowcondition:remind-handler";
    private static final String AMD_MODULE_KEY =
            "FECRU/workflow-condition-tutorial/remind-handler";
    private final ReviewService reviewService;
    private final ApplicationProperties applicationProperties;

    @Inject
    public CompletedReviewerWorkflowCondition(@ComponentImport ReviewService reviewService, @ComponentImport ApplicationProperties applicationProperties) {
        this.reviewService = reviewService;
        this.applicationProperties = applicationProperties;
    }

    public ValidationResult validateTransition(WorkflowTransitionContext transitionContext) {
        if (isCloseReview(transitionContext.getTransitionAction())) {
            return doValidate(transitionContext.getReviewId());
        }
        return ValidationResult.ok();
    }

    private boolean isCloseReview(String transitionAction) {
        return ReviewService.Action.Close.getActionString().equals(transitionAction);
    }

    private ValidationResult doValidate(PermId<ReviewData> reviewId) {
        if (isAtLeastOneReviewerCompleted(reviewId)) {
            return ValidationResult.ok();
        }
        return ValidationResult.error(CompletedReviewerWorkflowCondition.class.getSimpleName(),
                new ResultMessage("No one completed the review",
                        "This review can't be closed until at least one reviewer completes it",
                        "This review can't be closed until at least one reviewer completes it",
                        Lists.newArrayList(
                                ResultAction.buildUrlAction("See users", applicationProperties.getBaseUrl() + "/users"),
                                ResultAction.buildJavaScriptAction("Send reminders",
                                        MODULE_KEY,
                                        AMD_MODULE_KEY)
                        )));
    }

    private boolean isAtLeastOneReviewerCompleted(PermId<ReviewData> reviewId) {
        return reviewService.getAllReviewers(reviewId).isEmpty()
                || !reviewService.getCompletedReviewers(reviewId).isEmpty();
    }
}
