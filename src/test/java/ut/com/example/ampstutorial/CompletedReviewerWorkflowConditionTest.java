package ut.com.example.ampstutorial;

import com.atlassian.crucible.spi.PermId;
import com.atlassian.crucible.spi.data.ReviewData;
import com.atlassian.crucible.spi.data.ReviewerData;
import com.atlassian.crucible.spi.services.ReviewService;
import com.atlassian.crucible.workflow.ResultSeverity;
import com.atlassian.crucible.workflow.ValidationResult;
import com.atlassian.crucible.workflow.WorkflowCondition;
import com.atlassian.crucible.workflow.WorkflowTransitionContext;
import com.atlassian.sal.api.ApplicationProperties;
import com.example.ampstutorial.CompletedReviewerWorkflowCondition;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CompletedReviewerWorkflowConditionTest {
    private static final PermId<ReviewData> REVIEW_ID = new PermId<>("CR-1");

    private WorkflowCondition completedReviewer;

    private ReviewService reviewService;
    private ApplicationProperties applicationProperties;

    @BeforeEach
    public void setUp() throws Exception {
        reviewService = mock(ReviewService.class);
        applicationProperties = mock(ApplicationProperties.class);
        completedReviewer = new CompletedReviewerWorkflowCondition(reviewService, applicationProperties);
    }

    @Test
    @DisplayName("When review is being approved validation result should be OK")
    void testWhenApprovingReview() {
        final ValidationResult result = completedReviewer.validateTransition(new WorkflowTransitionContext(REVIEW_ID,
                ReviewService.Action.Approve.getActionString(),
                null));

        assertEquals(ResultSeverity.OK, result.getResultSeverity());
    }

    @Test
    @DisplayName("When review is being closed and all reviewers completed review validation result should be OK")
    void testWhenClosingAndAllReviewersCompleted() {
        when(reviewService.getAllReviewers(REVIEW_ID)).thenReturn(Collections.singleton(new ReviewerData()));
        when(reviewService.getCompletedReviewers(REVIEW_ID)).thenReturn(Collections.singleton(new ReviewerData()));

        final ValidationResult result = completedReviewer.validateTransition(new WorkflowTransitionContext(REVIEW_ID,
                ReviewService.Action.Close.getActionString(),
                null));

        assertEquals(ResultSeverity.OK, result.getResultSeverity());
    }

    @Test
    @DisplayName("When review is being closed and not all reviewers completed review validation result should be error")
    void testWhenClosingAndNotAllReviewersCompleted() {
        when(reviewService.getAllReviewers(REVIEW_ID)).thenReturn(Collections.singleton(new ReviewerData()));
        when(reviewService.getCompletedReviewers(REVIEW_ID)).thenReturn(Collections.emptySet());

        final ValidationResult result = completedReviewer.validateTransition(new WorkflowTransitionContext(REVIEW_ID,
                ReviewService.Action.Close.getActionString(),
                null));

        assertAll("Completed Reviewer Workflow Condition",
                () -> assertEquals(ResultSeverity.ERROR, result.getResultSeverity()),
                () -> assertEquals(CompletedReviewerWorkflowCondition.class.getSimpleName(), result.getResultKey()));
    }

    @Test
    @DisplayName("When review is being closed and it has no reviewers validation result should be OK")
    void testWhenClosingAndNoReviewers() {
        when(reviewService.getAllReviewers(REVIEW_ID)).thenReturn(Collections.emptySet());
        when(reviewService.getCompletedReviewers(REVIEW_ID)).thenReturn(Collections.emptySet());

        final ValidationResult result = completedReviewer.validateTransition(new WorkflowTransitionContext(REVIEW_ID,
                ReviewService.Action.Close.getActionString(),
                null));

        assertEquals(ResultSeverity.OK, result.getResultSeverity());
    }
}